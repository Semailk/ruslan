<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class UserProfile
 * @package App\Models
 * @property string $first_name
 * @property string $last_name
 * @property int $user_id
 */
class UserProfile extends Model
{
    protected $table = 'user_profiles';

    protected $fillable = ['first_name', 'last_name'];
}
