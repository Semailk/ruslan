<?php

namespace App\Http\Controllers;

use App\Forum;
use App\Images;
use App\Models\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Promise\all;

class WorkController extends Controller
{
    public function index()
    {
        return view('proekt.index');
    }

    public function we()
    {
        return view('proekt.page-2');
    }

    public function service()
    {
        return view('proekt.page-3');
    }

    public function news()
    {
        return view('proekt.page-4');
    }

    public function projects()
    {
        return view('proekt.projects');
    }

    public function contact()
    {
        return view('proekt.page-5-contacts');
    }

    public function gallery(Request $request)
    {
        $array = $request->user()->images->pluck('img');
        $test = $request->user()->images()->paginate(10);

        return view('proekt.gallery', ['path' => '', 'array' => $array, 'test' => $test]);
    }

    public function upload(Request $request)
    {
        $path = $request->file('img')->store('upload', 'public');
        $request->user()->images()->create(['img' => $request->file('img')->hashName()]);

        return redirect('gallery')->with('path', $path);
    }

    public function forum()
    {
        $test = Forum::paginate(5);
        return view('proekt.forum',['test' => $test]);
    }

    public function comment(Request $request)
    {
        $test = Forum::all();
      $request->user()->forums()->create($request->input());
        return redirect(route('forum'));
    }

    public function show($id)
    {
        $show = UserProfile::find($id);
        return view('proekt.show');
    }

}
