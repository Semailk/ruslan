<?php

namespace App\Http\Controllers;

use App\Models\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;


class UserProfileController extends Controller
{

    public function index()
    {
        $fileProfile = UserProfile::all();
        return view('proekt.UserProfile', ['fileProfile' => $fileProfile]);
    }

    public function create(Request $request)
    {
        $user = $request->user();
        $userProfile = new UserProfile;
        $userProfile->first_name = $request->input('first_name');
        $userProfile->last_name = $request->input('last_name');
        $userProfile->user_id = $user->id;
        $userProfile->save();

        return redirect(route('profile.index'));
    }

    public function delete($id)
    {
        UserProfile::find($id)->delete();
        return redirect(route('profile.index'));
    }

    public function edit($id)
    {
        $user = UserProfile::find($id);
        return view('proekt.edit', ['user' => $user]);
    }

    public function update(Request $request, $id)
    {
        $test = UserProfile::find($id);

        $test->first_name = $request->first_name;
        $test->last_name = $request->last_name;
        $test->save();
        return redirect(route('profile.index'));
    }
}
