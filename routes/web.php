<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware' => 'auth'
], function(){
    Route::get('/gallery', 'WorkController@gallery')->name('gallery');

    Route::post('/image/upload', 'WorkController@upload')->name('upload');

    Route::get('/forum', 'WorkController@forum')->name('forum');

    Route::post('/save/comment', 'WorkController@comment')->name('comment');
});

Route::get('/','WorkController@index')->name('index');

Route::get('/we','WorkController@we')->name('we');

Route::get('/services', 'WorkController@service')->name('service');

Route::get('/news', 'WorkController@news')->name('news');

Route::get('/projects', 'WorkController@projects')->name('projects');

Route::get('/contact', 'WorkController@contact')->name('contact');

Route::get('/user/profile', 'UserProfileController@index')->name('profile.index');

Route::post('/user/create', 'UserProfileController@create')->name('profile.create');

Route::get('/user/{id}/delete', 'UserProfileController@delete')->name('profile.delete');

Route::get('user/{id}/edit', 'UserProfileController@edit')->name('profile.edit');

Route::get('user/{id}/update', 'UserProfileController@update')->name('profile.update');

Route::get('user/{id}/show', 'UserProfileController@show')->name('profile.show');

Route::get('/home', 'HomeController@index')->name('home');


\Illuminate\Support\Facades\Auth::routes();



Route::get('/project-1','ProjectController@oneProj')->name('project-1');
Route::get('/project-2','ProjectController@twoProj')->name('project-2');
Route::get('/project-3','ProjectController@threeProj')->name('project-3');
Route::get('/project-4','ProjectController@fourProj')->name('project-4');
Route::get('/project-5','ProjectController@fiveProj')->name('project-5');


