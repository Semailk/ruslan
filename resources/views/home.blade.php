@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
<button class="btn-warning" type="submit"><a href="/">Главная страница</a> </button>
                    {{ __('You are logged in!')}}
                    <?php setcookie("TestCookie", Auth::user()->name, time()+3600); ?>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
