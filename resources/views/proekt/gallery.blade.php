@extends('layout')
@section('content')
    <style>
        body {
            background: linear-gradient(90deg, #FFD7D7, #00a99a);
        }

        .gallery {
            width: 1440px;
            display: flex;
        }

        .gallery-img {
            width: 1050px;
        }
    </style>
    <div class="gallery">
        <div class="gallery-img">
{{--                @foreach($array as $value)--}}
{{--                    <img src="{{asset('storage/'.$value)}}" width="100px" height="100px">--}}
{{--                @endforeach--}}

            @foreach($test as $tes)
                <img src="{{asset('/storage/'.$tes->img)}}" width="200px" height="200px">
            @endforeach

        </div>

        @if (Auth::check())

            <div style="margin-top: 50px;" class="">
                <form style=" background: linear-gradient(90deg,honeydew,#00a99a); border: 5px solid #FFD7D7;"
                      action="{{route('upload')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <h3 style="font-family: 'Abyssinica SIL';text-align: center"><label>Загрузка картинки</label></h3>
                    <div class="form-group">
                        <input class="form-control-file" type="file" name="img">
                    </div>
                    <button class="btn btn-success" type="submit">Отправить</button>
                </form>
            </div>

        @else
            <div style="margin-top: 50px;" class="">
                <form style=" justify-content: center;flex-wrap: wrap;display: flex ;background: linear-gradient(90deg,honeydew,#00a99a); border: 5px solid #FFD7D7;"
                      action="{{route('upload')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <h5 style="font-family: 'Abyssinica SIL';text-align: center"><label>Автарезуйтесь для загрузки
                            изображения.</label></h5>
                    <a href="/login"> <button style="width: 120px;font-size: 25px;" class="btn-warning" type="button">Войти</button></a>
                </form>
            </div>
        @endif
    </div>
    {{$test->links()}}
@endsection
