<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
<form class="form-control" method="post" action="{{route('profile.create')}}" enctype="multipart/form-data">
    @csrf
    <input class="form-control" type="text" name="first_name">
    <input class="form-control" type="text" name="last_name">
    <button type="submit">Отправит</button>
</form>
</div>
<div style="margin-top: 100px;" class="box">
@foreach($fileProfile as $value)

    <div style="display: flex;justify-content: space-around;" class="form-group">
        <div class=""><h3>{{ $value->first_name }}</h3></div>
        <div class=""> <h3>{{$value->last_name}}</h3></div>
        <div class=""> <a href="/user/{{$value->id}}/delete">Удалить</a></div>
            <div class="">   <a href="/user/{{$value->id}}/edit">Изменить</a></div>
            <div class="">   <a href="/user/{{$value->id}}/show">Просмотреть</a></div>
    </div>
@endforeach
</div>
</body>
</html>

