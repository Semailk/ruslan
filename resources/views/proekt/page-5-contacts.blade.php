@extends('layout')
@section('content')
    <div class="page-5">
        <div class="page-5-content">
            <div class="page-5-progInfo">
                <img id="vk" src="../image/artur.png" width="180" height="200px">
                <b>Артур Фазылов</b>
                <h2>Возраст 21 год</h2>
                <h2>Разработчик Frontend и Backend</h2>
                <p>Опыт работы 5 лет, опытный программист во всех направлениях.Работа в популярных компаниях.
                    Куратор в сфере IT.Директор компании 'Успешная группа'.</p>
                <a href="https://vk.com/id190363469"> <img src="../image/vk.png" width="50" height="50"></a>
                <a href="https://github.com/Artur-Developer"> <img src="../image/git.png" width="50" height="50"></a>
                <a href="https://www.instagram.com/artur.fazylov/"> <img src="../image/inst.png" width="50" height="50"></a>
            </div>
            <div class="page-5-progInfo">
                <img src="../image/jeka.png" width="180" height="200px">
                <b>Евгений Красоткин</b>
                <h2>Возраст 21 год</h2>
                <h2>Разработчик Frontend</h2>
                <p>Опыт работы 3 года, опытный программист во всех направлениях.Работа над серьезными проектами в
                    команде.
                    Настоящий специалист своего дела.Зам директор 'Успешная группа'.</p>
                <a href="https://vk.com/tearofenemy"> <img src="../image/vk.png" width="50" height="50"></a>
                <a href="https://github.com/tearofenemy"> <img src="../image/git.png" width="50" height="50"></a>
                <a href="https://www.instagram.com/tearofenemy/"> <img src="../image/inst.png" width="50"
                                                                       height="50"></a>
            </div>
            <div class="page-5-progInfo">
                <img src="../image/alik.png" width="180" height="200px">
                <b>Руслан Алиев</b>
                <h2>Возвраст 25 лет</h2>
                <h2>Backend разработчик</h2>
                <p>Опыт работы в IT 1 год, Работа в одиночку и в команде над серьезными проектами.
                    Уверенный спицеолист своего дела.Работник компании 'Успешная группа'.</p>
                <a href="https://vk.com/id15341621"> <img src="../image/vk.png" width="50" height="50"></a>
                <a href="https://github.com/semailk"> <img src="../image/git.png" width="50" height="50"></a>
                <a href="https://www.instagram.com/semailk/"> <img src="../image/inst.png" width="50" height="50"></a>
            </div>
        </div>
    </div>

@endsection

