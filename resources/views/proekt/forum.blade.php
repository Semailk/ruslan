@extends('layout')
@section('content')
    <style>
        body {
            background: #f3dbdb;
        }

        .forum {
            width: 1440px;
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        .box-text {
            margin-top: 50px;
            width: 800px;
            height: 150px;
            display: flex;
            background: #C4C4C4;
            flex-wrap: wrap;
            justify-content: center;
        }

        .form {
            display: flex;
            justify-content: center;
        }
    </style>

    <div class="forum">

        @foreach($test as $value)
            <div class="box-text">
                <h3 style="text-align: center;width: 100%;">{{$value->name}}</h3>
                <h6>{{$value->description}}</h6>
                {{$value->id}}
            </div>
        @endforeach
    </div>
    <div style="display: flex;justify-content: center;" class="">{{$test->links() ?? null}}</div>
    <div class="form">
        <form action="{{route('comment')}}" method="post">
            @csrf
            <label style="display: flex;justify-content: center"><h3>Сообщение</h3></label>
            <input style="width: 300px;" type="text" name="name" placeholder="Текст сообщения" class="form-control">
            <textarea style="margin:7px 0 7px 0;width: 300px" name="description" placeholder="Коментарий"
                      class="form-control"></textarea>
            <button type="submit" class="btn btn-primary">Отправить</button>
        </form>
    </div>

@endsection
