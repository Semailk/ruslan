<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style-page.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-page2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-page3.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-page4.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.projects.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-page5-contacts.css') }}">

    <title>Document</title>
</head>
<style>
    * {
        margin: 0;
        padding: 0;
    }

    a {
        color: black;
    }
</style>
<body>
<div style="width: 1440px;" class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 style="font-family: Z003;font-size: 30px;" class="my-0 mr-md-auto font-weight-normal">Успешная Компания</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <nav>
            <a class="p-2 text-dark" href="{{route('index')}}">Главная</a>
            <a class="p-2 text-dark" href="{{route('we')}}">О нас</a>
            <a class="p-2 text-dark" href="{{route('service')}}">Услуги</a>
            <a class="p-2 text-dark" href="{{route('news')}}">Интересное</a>
            <a class="p-2 text-dark" href="{{route('contact')}}">Контакты</a>
            <a class="p-2 text-dark" href="{{route('gallery')}}">Галерея</a>
            <a class="p-2 text-dark" href="{{route('forum')}}">Форум</a>
        </nav>
    </nav>
    <div class="btn-group" role="group" aria-label="..."></div>
    <div class="btn-group btn-group-sm mt-3" role="group" aria-label="...">
        @if (Route::has('login'))
            <div style="margin-bottom: 17px;" class="top-right links">
                @auth

                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a style="position: absolute;right: -5px;text-align: center;" class="dropdown-item"
                           href="/logout"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Выйти') }}

                        </a>

                        <form id="logout-form" action="/logout" method="POST" style="display: flex ;">
                            @csrf

                        </form>
                    </div>

                @else
                    <a href="{{ route('login') }}">Войти</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Регистрация</a>
                    @endif
                @endauth
            </div>
        @endif


    </div>
</div>


@yield('content')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
</body>
</html>
